const users = ['Jeff', 'Britta', 'Annie', 'Abed', 'Troy', 'Shirley', 'Pierce'];
const items = ['Table', 'Ball', 'Pen', 'Book'];

// #3
const addToUsers = function (user) {
	users.push(user);
};

addToUsers('Chang');
console.log(users);
//

// #4
const findItem = function (index) {
	return items[index];
};

const itemFound = findItem(2);
console.log(itemFound);
//

// #5
const deleteLastItem = function () {
	const lastItem = items.pop();
	return lastItem;
};

const deletedItem = deleteLastItem();
console.log(deletedItem);
//

// #6
const updateUsers = function (update, index) {
	users[index] = update;
};

updateUsers('Dean', 5);
console.log(users);
//

// #7
const deleteAllUsers = function () {
	users.length = 0;
};

deleteAllUsers();
console.log(users);
//

// #8
const checkIfEmpty = function () {
	return users.length === 0;
};

const isUserEmpty = checkIfEmpty();
console.log(isUserEmpty);
//
